/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;


/**
 *
 * @author felip
 */
public class Cuenta {
    private int numeroC;
    private Cliente cliente;
    private String fechaA;
    private String nombreB;
    private float porcentajeR;
    private float saldo;
    
    public Cuenta(){
        
    }
    
    public Cuenta(int numeroC, Cliente cliente, String fechaA, String nombreB, float porcentajeR, float saldo){
        this.numeroC = numeroC;
        this.cliente = cliente;
        this.fechaA = fechaA;
        this.nombreB = nombreB;
        this.porcentajeR = porcentajeR;
        this.saldo = saldo;
    }
    
    public Cuenta(Cuenta CU){
        CU.numeroC = numeroC;
        CU.cliente = cliente;
        CU.fechaA = fechaA;
        CU.nombreB = nombreB;
        CU.porcentajeR = porcentajeR;
        CU.saldo = saldo;
    }
    
    public void setNumeroC(int numeroC){
        this.numeroC = numeroC;
    }
    
    public void setCliente(Cliente cliente){
        this.cliente = cliente;
    }
    
    public void setFechaA(String fechaA){
        this.fechaA = fechaA;
    }
    
    public void setNombreB(String nombreB){
        this.nombreB = nombreB;
    }
    
    public void setPorcentaje(float porcentajeR){
        this.porcentajeR = porcentajeR;
    }
    
    public void setSaldo(float saldo){
        this.saldo = saldo;
    }
    
    public int getNumeroC(){
        return this.numeroC;
    }
    
    public Cliente getCliente(){
        return this.cliente;
    }
    
    public String getFechaA(){
        return this.fechaA;
    }
    
    public String getNombreB(){
        return this.nombreB;
    }
    
    public float getPorcentaje(){
        return this.porcentajeR;
    }
    
    public float getSaldo(){
        return this.saldo;
    }
    
    public void deposito(float cantidad){
        this.saldo = this.saldo + cantidad;
    }
    
    public boolean retiro(float cantidad){
        if(cantidad <= this.saldo){
            this.saldo = this.saldo - cantidad;
            return true;
        }
        else{
            return false;
        }
    }
    
    public float calculoR(){
        if(porcentajeR != 12){
            porcentajeR = 12;
        }
        porcentajeR /= 100;
        porcentajeR = porcentajeR * (this.saldo / 365) * 100;
        return this.porcentajeR;
    }
}
