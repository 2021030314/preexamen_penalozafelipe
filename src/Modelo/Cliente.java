/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author felip
 */
public class Cliente {
    private String nombreC;
    private String fechaNac;
    private String domicilio;
    private String sexo;
    
    public Cliente(){
        
    }
    
    public Cliente(String nombreC, String fechaNac, String domicilio, String sexo){
        this.nombreC = nombreC;
        this.fechaNac = fechaNac;
        this.domicilio = domicilio;
        this.sexo = sexo;
    }
    
    public Cliente(Cliente CT){
        CT.nombreC = nombreC;
        CT.fechaNac = fechaNac;
        CT.domicilio = domicilio;
        CT.sexo = sexo;
    }
    
    public void setNombre(String nombreC){
        this.nombreC = nombreC;
    }
    
    public void setFecha(String fechaNac){
        this.fechaNac = fechaNac;
    }
    
    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    
    public String getNombre(){
        return this.nombreC;
    }
    
    public String getFecha(){
        return this.fechaNac;
    }
    
    public String getDomicilio(){
        return this.domicilio;
    }
    
    public String getSexo(){
        return this.sexo;
    }
}
