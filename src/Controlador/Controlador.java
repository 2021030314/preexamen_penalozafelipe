/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Cliente;
import Modelo.Cuenta;
import Vista.dlgCuenta;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
/**
 *
 * @author felip
 */
public class Controlador implements ActionListener {
    private Cliente CT;
    private Cuenta CU;
    private dlgCuenta vista;
    
    private Controlador(Cliente CT, Cuenta CU, dlgCuenta vista){
       this.CT = CT;
       this.CU = CU;
       this.vista = vista;
       
       vista.btnDeposito.addActionListener(this);
       vista.btnGuardar.addActionListener(this);
       vista.btnLimpiar.addActionListener(this);
       vista.btnNuevo.addActionListener(this);
       vista.btnRetiro.addActionListener(this);
       vista.rbtnFem.addActionListener(this);
       vista.rbtnMasc.addActionListener(this);
       vista.btnLimpiar.addActionListener(this);
       vista.btnMostrar.addActionListener(this);
       vista.btnRend.addActionListener(this);
    }
    
    private void iniciarV(){
        vista.setTitle("-¡- CAJERO -¡-");
        vista.setSize(700, 600);
        vista.setVisible(true);
    }
    
    private void cleanText(){
        vista.txtNumCuenta.setText(null);
        vista.txtNombre.setText(null);
        vista.txtFechaNac.setText(null);
        vista.txtDomicilio.setText(null);
        vista.txtFechaA.setText(null);
        vista.txtPorcentaje.setText(null);
        vista.txtSaldo.setText(null);
        vista.txtBanco.setText(null);
        vista.txtCantidad.setText(null);
        vista.txtSaldoN.setText(null);
        vista.rbtnGrupo.clearSelection();
    }
    

    public static void main(String[] args) {
        Cuenta CU = new Cuenta();
        Cliente CT = new Cliente();
        dlgCuenta vista = new dlgCuenta(new JFrame(), true);
        Controlador CRT = new Controlador(CT, CU, vista);
        CRT.iniciarV();
      
    }

    @Override
    public void actionPerformed(ActionEvent e){ 
       if(e.getSource() == vista.btnGuardar){
           /*CT.setNombre(vista.txtNombre.getText());
           CU.setNombreB(vista.txtBanco.getText());
           CT.setDomicilio(vista.txtDomicilio.getText());*/
           int cont = 0;
           if(vista.txtNombre.getText().equals("") || vista.txtBanco.getText().equals("") || vista.txtDomicilio.getText().equals("")){
               JOptionPane.showMessageDialog(vista, "Por favor llene todos los campos");
           }
           else{
               CT.setNombre(vista.txtNombre.getText());
               CU.setNombreB(vista.txtBanco.getText());
               CT.setDomicilio(vista.txtDomicilio.getText());
               cont += 1;
           }
           if(vista.rbtnFem.isSelected()){
               CT.setSexo("Femenino");
           }
           else if(vista.rbtnMasc.isSelected()){
               CT.setSexo("Masculino");
           }
           try{
               CU.setNumeroC(Integer.parseInt(vista.txtNumCuenta.getText()));
               CT.setFecha(vista.txtFechaNac.getText());
               CU.setFechaA(vista.txtFechaA.getText());
               cont += 1;
           }
           catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
           }
           if(cont == 2){
               vista.btnMostrar.setEnabled(true);
           }
       }
       
       if(e.getSource() == vista.btnMostrar){
           vista.txtNumCuenta.setText(String.valueOf(CU.getNumeroC()));
           vista.txtNombre.setText(CT.getNombre());
           vista.txtFechaNac.setText(CT.getFecha());
           vista.txtDomicilio.setText(CT.getDomicilio());
           vista.txtBanco.setText(CU.getNombreB());
           vista.txtFechaA.setText(CU.getFechaA());
           vista.txtPorcentaje.setText(String.valueOf(CU.getPorcentaje()));
           vista.txtSaldo.setText(String.valueOf(CU.getSaldo()));
           if(CT.getSexo().equals("Masculino")){
               vista.rbtnMasc.setSelected(true);
           }
           if(CT.getSexo().equals("Femenino")){
               vista.rbtnFem.setSelected(true);
           }
       }
       
       if(e.getSource() == vista.btnNuevo){
           vista.btnRend.setEnabled(false);
           vista.btnGuardar.setEnabled(false);
           vista.btnMostrar.setEnabled(false);
           vista.btnLimpiar.doClick();
           CT.setDomicilio(null);
           CT.setFecha(null);
           CT.setNombre(null);
           CT.setSexo(null);
           CU.setFechaA(null);
           CU.setNombreB(null);
           CU.setNumeroC(0);
           CU.setSaldo(0);
           vista.btnLimpiar.setEnabled(true);
           vista.btnRend.setEnabled(true);
           vista.txtNumCuenta.setEnabled(true);
           vista.txtNombre.setEnabled(true);
           vista.txtFechaNac.setEnabled(true);
           vista.txtDomicilio.setEnabled(true);
           vista.txtFechaA.setEnabled(true);
           vista.txtBanco.setEnabled(true);
           vista.rbtnMasc.setEnabled(true);
           vista.rbtnFem.setEnabled(true);
           vista.txtCantidad.setEnabled(true);     
           CU.setPorcentaje(12);
       }
       
       if(e.getSource() == vista.btnLimpiar){
           cleanText();
       }
       
       if(e.getSource() == vista.btnDeposito){
           try{
               CU.deposito(Float.parseFloat(vista.txtCantidad.getText()));
               vista.txtSaldoN.setText(String.valueOf(CU.getSaldo())); 
           }
           catch(NumberFormatException ex2){
               JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage()); 
           }
           vista.btnDeposito.setEnabled(false);
       }
       
       if(e.getSource() == vista.btnRend){
           vista.txtPorcentaje.setText(String.valueOf(CU.calculoR()));
       }
       
       if(e.getSource() == vista.btnRetiro){
           boolean option = CU.retiro(Float.parseFloat(vista.txtCantidad.getText()));
           if(option == true){
               vista.txtSaldoN.setText(String.valueOf(CU.getSaldo()));
           }
           else{
               JOptionPane.showMessageDialog(vista, "Cantidad insuficiente");
           }
       }
    }
}
